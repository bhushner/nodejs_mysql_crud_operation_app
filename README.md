1. import /execute "database_creation.sql" file in mysql  
 eg. $ mysql -u username -p database_name < database_creation.sql
2. run node project (at port 5000 )
open link : http://localhost:5000/employees

* use bellow api's for CRUD Operations.

1. Get all employees
method: GET	URL:	http://localhost:5000/employees 

2. Get Employee by id 
method: GET	URL:	http://localhost:5000/employees/[id]

eg. get employee details of id 1
method:GET	URL:	http://localhost:5000/employees/1

3. Delete employee by id
method:DELETE	URL:	http://localhost:5000/employees/[id]

eg. delete employee details of id 1
method:DELETE	URL:	http://localhost:5000/employees/1

4. Insert Employee
method:POST	URL:	http://localhost:5000/employees

BODY
{
"EmpID":0,
"Name":"Rajpal Yadav",
"EmpCode": "EMP15",
"Salary": 30000
}


5. Update Employee
method:POST	URL:	http://localhost:5000/employees
eg. employee(6,"Rajpal Yadav","EMP15",10000)
BODY
{
"EmpID":6,
"Name":"Rajkumar Rao",
"EmpCode": "EMP15",
"Salary": 66000
}

