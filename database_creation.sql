create database IF NOT EXISTS EmployeeDB ;
use EmployeeDB;
Drop table if exists employee;
create table IF NOT EXISTS employee(
EmpId int(11) not null auto_increment,
Name varchar(45) default null,
EmpCode varchar(45) default null,
Salary int(11) default null,
primary key (EmpId)
);

insert into employee values (1, "Akshay Kumar","EMP11",20123 ),(2, "Ajay Devgon","EMP12",22003 ),(3, "Sunil Shetty","EMP13",21000 ),(4,"Ashok Saraf","EMP14",20999);
/* procedure for Edit or Add Employee*/
DROP PROCEDURE IF EXISTS EmployeeAddOrEdit;
DELIMITER $$
CREATE PROCEDURE  `EmployeeAddOrEdit`(
IN _EmpID int,
IN _Name varchar(45),
IN _EmpCode varchar(45),
IN _Salary int
)
BEGIN 
	IF EmpId = 0 THEN 
		INSERT INTO employee(Name,EmpCode,Salary) values (_Name,_EmpCode,_Salary);
		SET _EmpID = LAST_INSERT_ID();
	ELSE
		UPDATE Employee
		SET Name = _Name,
		EmpCode= _EmpCode,
		Salary=  _Salary
		WHERE EmpID= _EmpID;
	END IF;
	SELECT _EmpID AS 'EmpId';
END $$
DELIMITER ;


select * from employee;
