//const { json } = require('express/lib/response');
const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');
app.use(bodyparser.json());


const mysqlConnection = mysql.createConnection({
    host:"localhost",
    user:"bhushan",
    password:"iauro100",
    database:"EmployeeDB",
    multipleStatements: true
});

mysqlConnection.connect((err)=> {
    if(!err)
        console.log("DB Connection Succeded.");
    else
        console.log("DB Connection failed.\n Error : "+JSON.stringify(err, undefined, 2 ));

});
const PORT = 5000;
app.listen(PORT, ()=>console.log(`server is Running at port no. ${PORT}`));

app.get('/',(req,res)=>{
    res.send("Welcome");
})
//GET all employees
app.get('/employees',(req,res)=>{
    mysqlConnection.query('SELECT * FROM employee',(err,rows,fields)=>{
        if(!err)
        {res.send(rows);
        console.log(rows);
        }else
        console.log(err);
    })
})


//GET an employee by id
app.get('/employees/:id',(req,res)=>{
    mysqlConnection.query('SELECT * FROM employee WHERE EmpId = ? ',[req.params.id],(err,rows,fields)=>{
        if(!err)
        {res.send(rows);
        console.log(rows);
        }else
        console.log(err);
    })
})

//DELETE an employee
app.delete('/employees/:id',(req,res)=>{
    mysqlConnection.query('DELETE FROM employee WHERE EmpId = ? ',[req.params.id],(err,rows,fields)=>{
        if(!err)
        {res.send("DELETED SUCCESSFULLY !");
        console.log("DELETED SUCCESSFULLY !");
        }else
        console.log(err);
    })
})

//Insert an employee
app.post('/employees',(req,res)=>{
    let emp = req.body;
    var sql = "SET @EmpID = ? ; SET @Name= ? ; SET @EmpCode = ? ; SET @Salary=? ;\
     CALL EmployeeAddOrEdit(@EmpID,@Name,@EmpCode,@Salary);";

    mysqlConnection.query(sql,[emp.EmpID,emp.Name,emp.EmpCode,emp.Salary],(err,rows,fields)=>{
        if(!err){
            rows.forEach(element => {
                if(element.constructor == Array)
                res.send("InsertedEMployee ID is : "+element[0].EmpID);
                console.log("Employee added with id: "+element[0].EmpID);
            });
        
        }else
        console.log(err);
    })
})

//update an employee
app.put('/employees',(req,res)=>{
    let emp = req.body;
    var sql = "SET @EmpID = ? ; SET @Name= ? ; SET @EmpCode = ? ; SET @Salary=? ;\
     CALL EmployeeAddOrEdit(@EmpID,@Name,@EmpCode,@Salary);";

    mysqlConnection.query(sql,[emp.EmpID,emp.Name,emp.EmpCode,emp.Salary],(err,rows,fields)=>{
        if(!err){
                res.send("Employee updated successfully ");
                console.log("Employee updated successfully ");
            
        
        }else
        console.log(err);
    })
})
